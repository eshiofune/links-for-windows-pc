'''
Main module for the Linker project.
'''

import os

import glob
import winshell

import structures


class Linker:
    '''
    Main class for the Linker project.
    '''
    def __init__(self, *args):
        pass

    def work_on_list(input_list):
        '''
        Works on input_list and returns a store containing all distinct items in
        input_list and an output_list which replaces the items in input_list
        with their indices in store.
        '''
        output_list = []
        store = []
        for item in input_list:
            for each_item in store:
                if each_item == item:
                    item = each_item
                    break
            else:
                store.append(item)
            output_list.append(store.index(item))
        return { "store": store, "output_list": output_list }

    def work_on_folder(input_folder_path, dst_folder_path, extension=""):
        '''
        Works on input_folder and returns a store containing all distinct files in
        input_folder and an output_folder which replaces the files in input_folder
        with their shortcuts to their equivalents in store.
        '''
        input_folder = structures.Folder(input_folder_path)

        output_folder = structures.Folder(dst_folder_path)
        output_folder.create() if not output_folder.exists() else False

        store = structures.Folder(os.path.join(dst_folder_path, "store"))
        store.create() if not store.exists() else False

        for file in input_folder.get_files():
            file_name = file.get_name()
            if not file.path.endswith(extension):
                continue
            for each_file in store.get_files():
                if each_file == file:
                    file = each_file
                    break
            else:
                file.move_to(store.path)
            file.create_shortcut_in(output_folder.path, shortcut_name=file_name)
        return { "store": store.get_name(), "output_folder": output_folder.get_name() }

    def reverse(store, input_list):
        '''
        Reverses the effect of work_on().
        '''
        output_list = []
        for each_index in input_list:
            output_list.append(store[each_index])
        return output_list

    def reverse_for_folder(folder_path):
        '''
        Reverses the effect of work_on_folder().
        '''
        folder = structures.Folder(folder_path)
        output_folder = structures.Folder(os.path.join(folder_path, "original form/"))
        output_folder.create() if not output_folder.exists() else False

        for lnk in glob.glob(os.path.join(folder_path, "*.lnk")):
            shortcut = winshell.shortcut(lnk)
            file = structures.File(shortcut.path).move_to(output_folder.path)

        print("Done.")

if __name__ == "__main__":
    if input("folder or list?") == "folder":
        print(
            Linker.work_on_folder(
                input("Input folder path >> "),
                input("Output folder path >> "),
                input("Extension >> ")
            )
        )