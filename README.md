Python application for file deduplication.

* Quick summary: Removes duplicates of files in a folder and replaces them (original as well as duplicates) with links (which consume less space). Each original (distinct) file is kept in a new folder called store/, while the links are kept in a different folder called output/. The original folder is left untouched.

* Version: 1.00

* Summary of set up:
- Install winshell
- Run the main.py file.

* Dependencies: winshell, structures (included in this repo).

* Repo owner or admin: Ehiorobo Evans (Bitbucket username - eshiofune)